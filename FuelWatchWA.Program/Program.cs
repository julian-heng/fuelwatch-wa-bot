﻿using Microsoft.Extensions.DependencyInjection;

using CommandLine;
using Discord.WebSocket;

using FuelWatchWA.Bot;
using FuelWatchWA.Bot.Mappers;
using FuelWatchWA.Bot.OneShot;
using FuelWatchWA.Bot.SlashCommands;
using FuelWatchWA.Core.Mappers;
using FuelWatchWA.Core.Net;
using FuelWatchWA.Core.Services;
using FuelWatchWA.Models.Program;

namespace FuelWatchWA.Program
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var serviceProvider = ConfigureServices().BuildServiceProvider();
            await Parser.Default.ParseArguments<BotOptions, ListOptions>(args).MapResult(
                async (BotOptions o) => {
                    var bot = serviceProvider.GetService<FuelWatchWABot>();
                    if (bot != null)
                    {
                        await bot.RunAsync(o.Token ?? "");
                    }
                },
                async (ListOptions o) => {
                    var bot = serviceProvider.GetService<FuelWatchWABotBatchList>();
                    if (bot != null)
                    {
                        bot.GuildChannels = ParseGuildChannels(o.GuildChannels ?? new List<string>());
                        bot.Suburbs = o.Suburbs ?? new List<string>();
                        await bot.RunAsync(o.Token ?? "");
                    }
                },
                e => Task.FromResult(1)
            );
        }

        private static IServiceCollection ConfigureServices()
        {
            return new ServiceCollection()
                .AddTransient<DiscordSocketClient, DiscordSocketClient>()
                .AddTransient<IStationMapper, StationMapper>()
                .AddTransient<HttpClient, HttpClient>()
                .AddTransient<FuelWatchHttpClient, FuelWatchHttpClient>()
                .AddTransient<IEmbedMapper, EmbedMapper>()
                .AddTransient<IOptionsMapper, OptionsMapper>()
                .AddTransient<IStationService, StationService>()
                .AddTransient<ListCommand, ListCommand>()
                .AddTransient<FuelWatchWABot, FuelWatchWABot>()
                .AddTransient<FuelWatchWABotBatchList, FuelWatchWABotBatchList>();
        }

        private static IEnumerable<(ulong, ulong)> ParseGuildChannels(IEnumerable<string> guildChannels)
        {
            return guildChannels.Select(i =>
            {
                var a = i.Split(':', 2);
                if (a.Length != 2)
                {
                    return (0UL, 0UL);
                }

                if (!ulong.TryParse(a[0], out var guild))
                {
                    guild = 0UL;
                }
                if (!ulong.TryParse(a[1], out var channel))
                {
                    channel = 0UL;
                }
                return (guild, channel);
            });
        }
    }
}
