FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:8.0 as build-env
ARG TARGETARCH
WORKDIR /source
COPY . /source
RUN dotnet restore --arch $TARGETARCH
RUN dotnet publish --arch $TARGETARCH --no-restore --nologo -o /app

FROM mcr.microsoft.com/dotnet/runtime:8.0
COPY --from=build-env /app /app
WORKDIR /app
USER $APP_UID
ENV TOKEN=""
ENTRYPOINT dotnet FuelWatchWA.Program.dll bot
