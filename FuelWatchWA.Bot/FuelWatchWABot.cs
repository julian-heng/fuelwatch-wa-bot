using Discord.Net;
using Discord.WebSocket;

using FuelWatchWA.Bot.SlashCommands;
using FuelWatchWA.Models.Bot;

namespace FuelWatchWA.Bot
{
    public class FuelWatchWABot : Bot
    {
        private Dictionary<string, SlashCommand> _slashCommands = new Dictionary<string, SlashCommand>();

        public FuelWatchWABot(DiscordSocketClient client
            , ListCommand listCommand) : base(client)
        {
            _slashCommands.Add(listCommand.Name, listCommand);
        }

        public override async Task RunAsync(string token)
        {
            _client.Ready += ClientReady;
            _client.SlashCommandExecuted += SlashCommandHandler;
            await base.RunAsync(token);
            await Task.Delay(-1);
        }

        private async Task ClientReady()
        {
            try
            {
                var cmds = _slashCommands.Values.Select(c => c.GetSlashCommandBuilder().Build()).ToArray();
                await _client.BulkOverwriteGlobalApplicationCommandsAsync(cmds);
            }
            catch (HttpException)
            {

            }
        }

        private async Task SlashCommandHandler(SocketSlashCommand cmd)
        {
            await _slashCommands[cmd.Data.Name].Execute(cmd);
        }
    }
}
