using Discord.WebSocket;

using FuelWatchWA.Models;
using FuelWatchWA.Models.Bot;

namespace FuelWatchWA.Bot.Mappers
{
    public class OptionsMapper : IOptionsMapper
    {
        public Options MapToOptions(SocketSlashCommandData data)
        {
            var suburb = (string)(data.Options.FirstOrDefault(i => i.Name == "suburb")?.Value ?? "");
            if (!Enum.TryParse<Product>((string)(data.Options.FirstOrDefault(i => i.Name == "fueltype")?.Value ?? ""), out var product))
            {
                product = Product.ULP;
            }

            if (!Enum.TryParse<Day>((string)(data.Options.FirstOrDefault(i => i.Name == "day")?.Value ?? ""), out var day))
            {
                day = Day.TODAY;
            }

            var limit = (long)(data.Options.FirstOrDefault(i => i.Name == "limit")?.Value ?? 12L);

            return new Options
            {
                Suburb = suburb,
                Product = product,
                Day = day,
                Limit = limit,
            };
        }
    }
}
