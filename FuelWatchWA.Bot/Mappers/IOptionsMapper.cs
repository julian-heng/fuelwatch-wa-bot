using Discord.WebSocket;

using FuelWatchWA.Models.Bot;

namespace FuelWatchWA.Bot.Mappers
{
    public interface IOptionsMapper
    {
        Options MapToOptions(SocketSlashCommandData data);
    }
}
