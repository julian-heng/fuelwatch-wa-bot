using Discord;

using FuelWatchWA.Models;
using FuelWatchWA.Models.Core;

namespace FuelWatchWA.Bot.Mappers
{
    public interface IEmbedMapper
    {
        Embed MapToEmbed(IEnumerable<Station> stations
            , string suburb = ""
            , Product product = Product.ULP
            , Day day = Day.TODAY
            , long limit = 12);
    }
}
