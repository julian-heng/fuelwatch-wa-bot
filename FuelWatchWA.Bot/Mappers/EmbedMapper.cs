using System.Globalization;

using Discord;

using FuelWatchWA.Extensions;
using FuelWatchWA.Models;
using FuelWatchWA.Models.Core;

namespace FuelWatchWA.Bot.Mappers
{
    public class EmbedMapper : IEmbedMapper
    {
        public Embed MapToEmbed(IEnumerable<Station> stations
            , string suburb = ""
            , Product product = Product.ULP
            , Day day = Day.TODAY
            , long limit = 12)
        {
            var title = MakeTitle(suburb, product);
            var description = MakeDescription(day);

            var count = 0;
            var embedBuilder = new EmbedBuilder
            {
                Title = title,
                Description = description,
            };
            embedBuilder.WithFooter(Constants.FuelWatchWAFooter);

            foreach (var station in stations)
            {
                if (count == limit)
                {
                    break;
                }

                var fieldTitle = $"{count switch { 0 => "🥇", 1 => "🥈", 2 => "🥉", _ => $"{count + 1}.", }} {station.TradingName}";
                var fieldValue = station.EmbedString;
                embedBuilder.AddField(fieldTitle, fieldValue, true);
                count++;
            }

            var embed = embedBuilder.Build();
            return embed;
        }

        private string MakeTitle(string suburb = "", Product product = Product.ULP)
        {
            var title = "⛽ FuelWatch Prices ";
            if (!string.IsNullOrWhiteSpace(suburb))
            {
                title += $"for {CultureInfo.CurrentCulture.TextInfo.ToTitleCase(suburb)} ";
            }
            title += $"({product.GetDescription()})";
            return title;
        }

        private string MakeDescription(Day day = Day.TODAY)
        {
            var description = $"{DateTime.Today.AddDays((int)day):dddd, dd MMMM yyyy} ({day.GetDescription()})";
            return description;
        }
    }
}
