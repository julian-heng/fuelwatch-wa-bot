using Discord;
using Discord.WebSocket;

using FuelWatchWA.Bot.Mappers;
using FuelWatchWA.Core.Services;
using FuelWatchWA.Extensions;
using FuelWatchWA.Models;
using FuelWatchWA.Models.Bot;

namespace FuelWatchWA.Bot.SlashCommands
{
    public class ListCommand : SlashCommand
    {
        public override string Name => "list";
        public override string Description => "Fetches list of fuel prices";

        private IEmbedMapper _embedMapper;
        private IOptionsMapper _optionsMapper;
        private IStationService _stationService;

        public ListCommand(IEmbedMapper embedMapper
            , IOptionsMapper optionsMapper
            , IStationService stationService) : base()
        {
            _embedMapper = embedMapper;
            _optionsMapper = optionsMapper;
            _stationService = stationService;
        }

        protected override void _SetupSlashCommandBuilder(SlashCommandBuilder cmdBuilder)
        {
            cmdBuilder.AddOption("suburb", ApplicationCommandOptionType.String, "The suburb to search in");
            cmdBuilder.AddOption(new SlashCommandOptionBuilder()
                .WithName("fueltype")
                .WithDescription("The type of fuel to search for")
                .AddChoice(Product.ULP.GetDescription(), Product.ULP.ToString())
                .AddChoice(Product.PULP.GetDescription(), Product.PULP.ToString())
                .AddChoice(Product.DIESEL.GetDescription(), Product.DIESEL.ToString())
                .AddChoice(Product.BRAND_DIESEL.GetDescription(), Product.BRAND_DIESEL.ToString())
                .AddChoice(Product.LPG.GetDescription(), Product.LPG.ToString())
                .AddChoice(Product._98RON.GetDescription(), Product._98RON.ToString())
                .AddChoice(Product.E85.GetDescription(), Product.E85.ToString())
                .WithType(ApplicationCommandOptionType.String));
            cmdBuilder.AddOption(new SlashCommandOptionBuilder()
                .WithName("day")
                .WithDescription("The price as of the selected day")
                .AddChoice(Day.TODAY.GetDescription(), Day.TODAY.ToString())
                .AddChoice(Day.YESTERDAY.GetDescription(), Day.YESTERDAY.ToString())
                .AddChoice(Day.TOMORROW.GetDescription(), Day.TOMORROW.ToString())
                .WithType(ApplicationCommandOptionType.String));
            cmdBuilder.AddOption("limit", ApplicationCommandOptionType.Integer, "The number of results to return");
        }

        public async override Task Execute(SocketSlashCommand cmd)
        {
            await cmd.DeferAsync();
            var options = _optionsMapper.MapToOptions(cmd.Data);
            var stations = await _stationService.FetchStations(options.Suburb, options.Product, options.Day);
            stations = stations.OrderBy(i => i.Price);
            var embed = _embedMapper.MapToEmbed(stations, options.Suburb, options.Product, options.Day, options.Limit);
            await cmd.FollowupAsync(embed: embed);
        }
    }
}
