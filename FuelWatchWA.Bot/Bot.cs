using Discord;
using Discord.WebSocket;

namespace FuelWatchWA.Bot
{
    public abstract class Bot
    {
        protected DiscordSocketClient _client;

        public Bot(DiscordSocketClient client)
        {
            _client = client;
        }

        public virtual async Task RunAsync(string token)
        {
            _client.Log += Log;
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
