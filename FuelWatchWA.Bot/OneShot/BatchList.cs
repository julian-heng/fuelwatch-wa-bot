using Discord;
using Discord.WebSocket;

using FuelWatchWA.Bot.Mappers;
using FuelWatchWA.Core.Services;

namespace FuelWatchWA.Bot.OneShot
{
    public class FuelWatchWABotBatchList : FuelWatchWABotOneShot
    {
        public IEnumerable<string> Suburbs { get; set; } = new List<string>();

        private IEmbedMapper _embedMapper;
        private IStationService _stationService;

        public FuelWatchWABotBatchList(DiscordSocketClient client
            , IEmbedMapper embedMapper
            , IStationService stationService) : base(client)
        {
            _embedMapper = embedMapper;
            _stationService = stationService;
        }

        protected override async Task RunOneShotActionAsync(IEnumerable<SocketTextChannel> channels)
        {
            var embeds = new List<Embed>();
            foreach (var suburb in Suburbs)
            {
                var stations = await _stationService.FetchStations(suburb);
                stations.OrderBy(i => i.Price);
                var embed = _embedMapper.MapToEmbed(stations, suburb);
                embeds.Add(embed);
            }

            foreach (var channel in channels)
            {
                foreach (var embed in embeds)
                {
                    await channel.SendMessageAsync(embed: embed);
                }
            }
        }
    }
}
