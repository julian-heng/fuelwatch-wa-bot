using Discord.WebSocket;

namespace FuelWatchWA.Bot
{
    public abstract class FuelWatchWABotOneShot : Bot
    {
        public IEnumerable<(ulong, ulong)> GuildChannels { get; set; } = new List<(ulong, ulong)>();
        protected bool Completed { get; set; } = false;

        public FuelWatchWABotOneShot(DiscordSocketClient client) : base(client)
        {
        }

        public override async Task RunAsync(string token)
        {
            _client.Ready += RunOneShotActionOnChannelsAsync;
            await base.RunAsync(token);
            while (!Completed)
            {
                await Task.Delay(1000);
            }
        }

        private async Task RunOneShotActionOnChannelsAsync()
        {
            try
            {
                var channels = new List<SocketTextChannel>();
                foreach (var (guildId, channelId) in GuildChannels)
                {
                    var channel = _client.GetGuild(guildId)?.GetTextChannel(channelId);
                    if (channel != null)
                    {
                        channels.Add(channel);
                    }
                }

                await RunOneShotActionAsync(channels);
            }
            finally
            {
                Completed = true;
            }
        }

        protected abstract Task RunOneShotActionAsync(IEnumerable<SocketTextChannel> channels);
    }
}
