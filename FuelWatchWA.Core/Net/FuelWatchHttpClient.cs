using Microsoft.AspNetCore.WebUtilities;

using FuelWatchWA.Extensions;
using FuelWatchWA.Models;

namespace FuelWatchWA.Core.Net
{
    public class FuelWatchHttpClient
    {
        private HttpClient _client;

        public FuelWatchHttpClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> FetchXml(string suburb = "", Product product = Product.ULP,
            Day day = Day.TODAY)
        {
            var parameters = new Dictionary<string, string>()
            {
                { "Product", ((int)product).ToString() },
                { "Day", day.GetDescription() },
            };

            if (!string.IsNullOrWhiteSpace(suburb))
            {
                parameters.Add("Suburb", suburb);
            }

            var url = new Uri(QueryHelpers.AddQueryString(Constants.FuelWatchWABaseUrl, parameters));
            var res = await _client.GetAsync(url);
            res.EnsureSuccessStatusCode();

            return await res.Content.ReadAsStringAsync();
        }
    }
}
