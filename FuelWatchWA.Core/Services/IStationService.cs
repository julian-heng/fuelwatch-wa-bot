using FuelWatchWA.Models;
using FuelWatchWA.Models.Core;

namespace FuelWatchWA.Core.Services
{
    public interface IStationService
    {
        Task<IEnumerable<Station>> FetchStations(string suburb = ""
            , Product product = Product.ULP
            , Day day = Day.TODAY);
    }
}
