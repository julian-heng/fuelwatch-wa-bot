using FuelWatchWA.Core.Net;
using FuelWatchWA.Core.Mappers;
using FuelWatchWA.Models;
using FuelWatchWA.Models.Core;

namespace FuelWatchWA.Core.Services
{
    public class StationService : IStationService
    {
        private FuelWatchHttpClient _httpClient;
        private IStationMapper _stationMapper;

        public StationService(FuelWatchHttpClient httpClient
            , IStationMapper stationMapper)
        {
            _httpClient = httpClient;
            _stationMapper = stationMapper;
        }

        public async Task<IEnumerable<Station>> FetchStations(string suburb = ""
            , Product product = Product.ULP
            , Day day = Day.TODAY)
        {
            var xml = await _httpClient.FetchXml(suburb, product, day);
            var stations = _stationMapper.MapFromXml(xml);
            return stations;
        }
    }
}
