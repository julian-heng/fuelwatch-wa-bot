using FuelWatchWA.Models.Core;

namespace FuelWatchWA.Core.Mappers
{
    public interface IStationMapper
    {
        IEnumerable<Station> MapFromXml(string xml);
    }
}
