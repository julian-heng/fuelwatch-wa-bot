using System.Xml.Serialization;

using FuelWatchWA.Models.Core;

namespace FuelWatchWA.Core.Mappers
{
    public class StationMapper : IStationMapper
    {
        public IEnumerable<Station> MapFromXml(string xml)
        {
            var serializer = new XmlSerializer(typeof(Rss));
            using (var reader = new StringReader(xml))
            {
                return ((Rss)(serializer.Deserialize(reader) ?? new Rss()))?.Channel?.Stations ?? new List<Station>();
            }
        }
    }
}
