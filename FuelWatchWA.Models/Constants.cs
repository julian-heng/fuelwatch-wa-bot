namespace FuelWatchWA.Models
{
    public static class Constants
    {
        public const string FuelWatchWABaseUrl = @"https://www.fuelwatch.wa.gov.au/fuelwatch/fuelWatchRSS";
        public const string FuelWatchWAFooter = "Fuel prices provided by FuelWatch (www.fuelwatch.wa.gov.au)";
    }
}
