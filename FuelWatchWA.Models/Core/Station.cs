using System.Xml.Serialization;

namespace FuelWatchWA.Models.Core
{
    [XmlRoot("item")]
    public class Station
    {
        [XmlElement("trading-name")]
        public string? TradingName { get; set; }
        [XmlElement("price")]
        public float Price { get; set; }
        [XmlElement("latitude")]
        public float Latitude { get; set; }
        [XmlElement("longitude")]
        public float Longitude { get; set; }

        public string GoogleMapsLink => $"https://maps.google.com/?q={Latitude},{Longitude}";
        public string EmbedString => $"[**{Price}**]({GoogleMapsLink})";
    }
}
