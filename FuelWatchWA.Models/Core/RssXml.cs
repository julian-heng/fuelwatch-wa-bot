using System.Xml.Serialization;

namespace FuelWatchWA.Models.Core
{
    [XmlRoot("rss")]
    public class Rss
    {
        [XmlElement("channel")]
        public Channel? Channel { get; set; }
    }

    [XmlRoot("channel")]
    public class Channel
    {
        [XmlElement("item")]
        public List<Station>? Stations { get; set; }
    }
}
