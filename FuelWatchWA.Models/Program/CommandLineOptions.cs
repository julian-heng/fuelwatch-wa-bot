using CommandLine;

namespace FuelWatchWA.Models.Program
{
    public class CommandLineOptions
    {
        [Option('t', "token", HelpText = "The bot token")]
        public string? Token { get; set; } = Environment.GetEnvironmentVariable("TOKEN");
    }

    public class OneShotOptions : CommandLineOptions
    {
        [Option('c', "channels", Required = true, HelpText = "The guilds and channels to send messages to (key:pair)")]
        public IEnumerable<string>? GuildChannels { get; set; }
    }

    [Verb("bot", HelpText = "Run the main bot instance")]
    public class BotOptions : CommandLineOptions
    {
    }

    [Verb("list", HelpText = "Shows the fuel prices of specific areas once")]
    public class ListOptions : OneShotOptions
    {
        [Option("suburb", Required = true, HelpText = "Suburbs to list")]
        public IEnumerable<string>? Suburbs { get; set; }
    }
}
