using System.ComponentModel;

namespace FuelWatchWA.Models
{
    public enum Product : int
    {
        [Description("Unleaded Petrol")]
        ULP = 1,
        [Description("Premium Petrol")]
        PULP = 2,
        [Description("Diesel")]
        DIESEL = 4,
        [Description("LPG")]
        LPG = 5,
        [Description("98 RON")]
        _98RON = 6,
        [Description("E85")]
        E85 = 10,
        [Description("Brand Diesel")]
        BRAND_DIESEL = 11,
    }

    public enum Day : int
    {
        [Description("Today")]
        TODAY = 0,
        [Description("Yesterday")]
        YESTERDAY = -1,
        [Description("Tomorrow")]
        TOMORROW = 1,
    }
}
