namespace FuelWatchWA.Models.Bot
{
    public class Options
    {
        public string Suburb { get; set; } = "";
        public Product Product { get; set; } = Product.ULP;
        public Day Day { get; set; } = Day.TODAY;
        public long Limit { get; set; } = 12L;
    }
}
